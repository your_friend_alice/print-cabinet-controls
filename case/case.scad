pcb_size = [68, 53, 1.6];
component_h = 17;
solder_clearance = 3;

pcb_r = 4;
pcb_wiggle = 0.25;
roof_hole_size = [9, 50];
roof_hole_r = 1;
pcb_support_ring = 0.5;

threaded_insert_d = 4.5;
threaded_insert_h = 4.6;
pcb_screw_d = 3.4;
pcb_screw_head_h = 3.25;
pcb_screw_head_d = 5.5;
pcb_screw_flange = 1;

pot_shaft_l = 12;
pot_shaft_d = 7;
pot_flatted_shaft_l = 7;
pot_flatted_shaft_w = 4.5;
pot_shaft_wiggle = 0;
knob_base_l = 2;
knob_base_d = 11;
knob_d = 5;
knob_l = 25;
knob_chamfer = 1;
knob_min_wall = 1.4;

mounting_screw_d = 5;
mounting_screw_countersink = 3;
mounting_screw_margin = 3;

// height of the bottom of the port above the PCB
usb_hole_size = [9, 1.2, 5];
usb_hole_h = 12;
usb_hole_x = 47;

pot_hole_d = 9.5;
// height of the center of the potentiometer above the PCB
pot_hole_h = 6.3;
pot_y_pos = [38.5, 26.5, 14.5];

wall = 2;
floor_h = 0.5;
teeth_wiggle = 0.2;

$fn = 60;
tiny = 0.1;

pcb_h = pcb_size[2] + component_h + tiny;
mounting_screw_total_d = mounting_screw_d + 2*mounting_screw_countersink + 2*mounting_screw_margin;
bottom_h = max(
    solder_clearance + floor_h,
    pcb_screw_flange + pcb_screw_head_h,
    mounting_screw_countersink + floor_h,
);

module at_pcb_holes() for(y=[-1,1]) translate([pcb_size[0] - pcb_r, pcb_size[1]/2 + y*(pcb_size[1]/2-pcb_r), 0]) children();

module pcb_outline() {
    hull() {
        square([pcb_size[0]-pcb_r*2, pcb_size[1]]);
        at_pcb_holes() circle(r=pcb_r);
    }
}

module top_body_outline() {
    module m() offset(delta=pcb_wiggle + wall) pcb_outline();
    intersection() {
        m();
        translate([wall, 0]) m();
    }
}

module top_body() linear_extrude(pcb_size[2] + component_h + wall) top_body_outline();

module pcb_support_outline(left=true) {
    module p() offset(delta=pcb_wiggle + wall) pcb_outline();
    offset(r=-wall/2) offset(r=wall/2) {
        intersection() {
            p();
            union() for(y=[0,1]) translate([pcb_size[0], y*pcb_size[1]]) offset(r=pcb_r) square(pcb_r*2, center=true);
        }
        difference() {
            p();
            offset(delta=-pcb_support_ring) pcb_outline();
            if(!left) translate([-wall, 0]) offset(delta=-pcb_support_ring) pcb_outline();
        }
    }
}

module teeth(grow=0, chop=false) {
    l = bottom_h*2;
    for(x=[-1,1]) translate([-pcb_wiggle, pcb_size[1]/2 + x*(pcb_size[1]/2 + wall/2 + pcb_wiggle), 0]) intersection() {
        rotate([0, -45, 0]) cube([l*2, wall + grow*2, wall + grow*2], center=true);
        if(chop) {
            translate([0, 0, -l/4]) cube([l+grow*2, wall + grow*2, l/2], center=true);
        } else {
            cube([l+grow*2, wall + grow*2, l], center=true);
        }
    }
}

module top() {
    intersection() {
        top_body();
        difference() {
            union() {
                difference() {
                    top_body();
                    translate([0, 0, -tiny]) linear_extrude(pcb_h) offset(delta=pcb_wiggle) pcb_outline();
                }
                translate([0, 0, pcb_size[2]]) linear_extrude(component_h) pcb_support_outline(false);
            }
            for(y=pot_y_pos) translate([pcb_size[0] - pcb_support_ring - tiny, y, pcb_size[2] + pot_hole_h]) rotate([0, 90, 0]) cylinder(d=pot_hole_d, h=wall + pcb_support_ring + pcb_wiggle + tiny*2);
            translate([0, pcb_size[1]/2, -tiny]) linear_extrude(pcb_h + wall + tiny*2) offset(r=roof_hole_r) offset(delta=-roof_hole_r) square([roof_hole_size[0]*2, roof_hole_size[1]], center=true);
            at_pcb_holes() {
                cylinder(d=threaded_insert_d, h=threaded_insert_h + pcb_size[2]);
                cylinder(d=pcb_screw_d, h=pcb_h);
            }
            translate([0, pcb_size[1]/2, usb_hole_h]) {
                translate([-pcb_wiggle, 0, 0]) cube([
                    usb_hole_x + usb_hole_size[0]/2 + pcb_wiggle,
                    pcb_size[1]/2 + usb_hole_size[1],
                    usb_hole_size[2]
                ]);
                translate([usb_hole_x - usb_hole_size[0]/2, 0, 0]) cube([usb_hole_size[0], pcb_size[1]/2 + pcb_wiggle + wall + tiny, usb_hole_size[2]]);
            }
        }
    }
    teeth();
}

module bottom_outline() {
    translate([
        -pcb_wiggle - mounting_screw_total_d,
        -pcb_wiggle - wall
    ]) offset(r=mounting_screw_total_d/2) offset(delta=-mounting_screw_total_d/2) square([
        pcb_size[0] + pcb_wiggle*2 + wall + mounting_screw_total_d*2,
        pcb_size[1] + pcb_wiggle*2 + wall*2
    ]);
}

module at_mounting_screws() {
    for(x=[
        -pcb_wiggle - mounting_screw_total_d/2,
        pcb_size[0] + pcb_wiggle + wall + mounting_screw_total_d/2
    ]) for(y=[-1,1]) translate([x, pcb_size[1]/2 + y*(pcb_size[1]/2 + pcb_wiggle + wall - mounting_screw_total_d/2)]) children();
}

module bottom() {
    difference() {
        union() {
            difference() {
                union() {
                    translate([0, 0, -bottom_h]) linear_extrude(bottom_h) bottom_outline();
                    linear_extrude(pcb_size[2]) offset(r=wall/2) offset(delta=-wall/2) difference() {
                        translate([-wall*2 - pcb_wiggle, mounting_screw_total_d/2 - pcb_wiggle]) square([wall*2, pcb_size[1] + pcb_wiggle*2 - mounting_screw_total_d]);
                        at_mounting_screws() circle(d=mounting_screw_total_d);
                    }
                    teeth(wall, chop=true);
                }
                translate([0, 0, -bottom_h + floor_h]) linear_extrude(bottom_h) difference() {
                    pcb_outline();
                    pcb_support_outline();
                }
                at_pcb_holes() translate([0, 0, -bottom_h - tiny]) {
                    cylinder(d=pcb_screw_d, h=bottom_h + tiny*2);
                    cylinder(d=pcb_screw_head_d, h=pcb_screw_head_h + tiny);
                }
                at_mounting_screws() {
                    translate([0, 0, -bottom_h - tiny]) cylinder(d=mounting_screw_d, h=bottom_h + tiny*2);
                    translate([0, 0, -mounting_screw_countersink]) cylinder(d1=mounting_screw_d, d2=mounting_screw_d + mounting_screw_countersink*2 + tiny*2, h=mounting_screw_countersink + tiny);
                }
            }
        }
        teeth(teeth_wiggle);
    }
}

module knob() {
    bottom_d = pot_shaft_d + pot_shaft_wiggle*2 + knob_min_wall*2;
    intersection() {
        union() {
            h = knob_l - (knob_base_d - knob_d)/2 - knob_chamfer;
            cylinder(d=knob_base_d, h=h);
            translate([0, 0, h]) cylinder(d1=knob_base_d, d2=knob_d - knob_chamfer*2, h=(knob_base_d - knob_d)/2 + knob_chamfer);
        }
        difference() {
            union() {
                cylinder(d=knob_base_d, h=knob_base_l);
                cylinder(d=bottom_d, h=pot_shaft_l + knob_min_wall);
                translate([0, 0, pot_shaft_l + knob_min_wall]) cylinder(d1=bottom_d, d2=knob_d, h=(bottom_d - knob_d)/2);
                translate([-knob_min_wall/2, -knob_base_d/2, 0]) cube([knob_min_wall, knob_base_d/2, pot_shaft_l + knob_min_wall]);
                linear_extrude(knob_l) offset(r=-knob_min_wall/2) offset(delta=knob_min_wall/2) {
                    for(a=[0:20:360]) rotate(a) translate([0, knob_d/2]) rotate(45) square(knob_min_wall/4, center=true);
                    circle(d=knob_d);
                }
            }
            difference() {
                translate([0, 0, -tiny]) cylinder(d=pot_shaft_d + pot_shaft_wiggle*2, h=pot_shaft_l + tiny);
                translate([-pot_shaft_d, pot_flatted_shaft_w - pot_shaft_d/2 + pot_shaft_wiggle, pot_shaft_l - pot_flatted_shaft_l]) cube([pot_shaft_d*2, pot_shaft_d, pot_flatted_shaft_l + tiny]);
            }
        }
    }
}
translate([pcb_size[0] + pcb_wiggle + wall*2 + knob_base_d/2, 0]) knob();
top();
translate([0, 0, -bottom_h*2]) bottom();
