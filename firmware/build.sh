#!/usr/bin/env bash
set -eou pipefail
declare -a UPLOAD_ARGS=()
if [[ "$#" -gt 0 ]] && [[ "$1" == program ]]; then
    PORT="/dev/ttyACM0"
    if [[ $# -gt 1 ]]; then
        PORT="$2"
    fi
    UPLOAD_ARGS=(--upload --port "$PORT")
elif [[ "$#" -gt 0 ]] && [[ "$1" == first ]]; then
    UPLOAD_ARGS=(--upload --port "UF2 Board")
fi
HERE="$(dirname "$(readlink -f "$0")")"
EXE="$(readlink -f "$(which arduino-cli)")"
set -x; "$EXE" compile --fqbn rp2040:rp2040:rpipico "${UPLOAD_ARGS[@]}" "$HERE"
