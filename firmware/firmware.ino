#include <cmath>

const size_t buf_len = 10;
const size_t num_channels = 3;
const int32_t knob_hysteresis = 4096/10;
const int32_t freq = 25000;
const int32_t trim_bottom = 20;
const int32_t trim_top = 20;
const int filter = 4;

struct command {
    bool available;
    uint8_t index;
    float value;
};

struct command getCommand() {
    struct command result;
    result.available = false;
    char buf[buf_len];
    Serial.print("get ");
    Serial.println(Serial.available(), DEC);
    if(!Serial.available() > 0) {
        return result;
    }
    size_t read = Serial.readBytesUntil('=', buf, buf_len);
    if(!read > 0) {
        return result;
    }
    size_t i;
    if(buf[0] < 'A' || buf[0] > 'Z') {
        return result;
    }
    result.index=buf[0]-'A';
    if(result.index >= num_channels) {
        return result;
    }
    result.value = Serial.parseFloat();
    if(result.value == result.value) {
        result.available = true;
    }
    Serial.readBytesUntil('\n', buf, buf_len);
    return result;
}

class channel {
public:
    uint8_t adcPin;
    uint8_t pwmPin;
    bool manual;
    bool invert;
    int32_t cmdValue;
    int32_t limit;
    int32_t adcValue;
    int32_t lastAdcValue;
};

channel channels[num_channels];

void setup() {
    Serial.begin(9600);
    channels[0].pwmPin = 3;
    channels[0].adcPin=A2;
    channels[0].limit=4096/3;
    channels[0].invert=false;

    channels[1].pwmPin = 4;
    channels[1].adcPin=A1;
    channels[1].limit=4096;
    channels[1].invert=true;

    channels[2].pwmPin = 5;
    channels[2].adcPin=A0;
    channels[2].limit=4096;
    channels[2].invert=true;

    analogWriteFreq(freq);
    analogWriteResolution(16);
    analogReadResolution(12);

    for(size_t i=0; i<num_channels; i++) {
        pinMode(channels[i].adcPin, INPUT);
        pinMode(channels[i].pwmPin, OUTPUT);
        channels[i].manual = true;
        channels[i].adcValue = 0;
    }
}

void writeChannel(uint8_t i, int32_t value) {
    value *= channels[i].limit;
    value >>= 12 - 4; // divide by 4096 to make it a 12 bit number, multiply by 16 to make it a 16 bit number
    if(value >= (channels[i].limit << 4) - 1) {
        value = (channels[i].limit << 4) - 1;
    }
    if(channels[i].invert) {
        value = 0xffff - value;
    }
    Serial.print(" ");
    Serial.print(value, DEC);
    analogWrite(channels[i].pwmPin, value);
}

int32_t trimValue(int32_t value) {
    if(value >= 0xffff - trim_top) {
        value = 0xffff - trim_top;
    }
    value -= trim_bottom;
    if(value < 0) {
        value = 0;
    }
    value *= 4096 + trim_bottom + trim_top;
    value /= 4096;
    if(value >= 0xffff) {
        value = 0xffff;
    }
    return value;
}

void loop() {
    struct command cmd = getCommand();
    if(cmd.available) {
        Serial.print("Got command. Index: ");
        Serial.print(cmd.index, DEC);
        Serial.print(" Value: ");
        Serial.print(cmd.value, DEC);
        Serial.println("");
        channels[cmd.index].manual = false;
        channels[cmd.index].lastAdcValue = channels[cmd.index].adcValue;
        channels[cmd.index].cmdValue = int32_t(cmd.value * 4096);
    }
    Serial.print("Outputs: ");
    for(size_t i=0; i<num_channels; i++) {
        channels[i].adcValue = channels[i].adcValue - (channels[i].adcValue >> filter) + (analogRead(channels[i].adcPin) >> filter);
        if(!channels[i].manual) {
            int32_t difference = channels[i].adcValue - channels[i].lastAdcValue;
            if(difference<0) {
                difference *= -1;
            }
            if(difference > knob_hysteresis)  {
                channels[i].manual = true;
            }
        }
        if(channels[i].manual) {
            writeChannel(i, trimValue(channels[i].adcValue));
        } else {
            writeChannel(i, channels[i].cmdValue);
        }
    }
    delay(10);
    Serial.println("");
}
