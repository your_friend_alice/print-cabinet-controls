#!/usr/bin/env bash
PORT=/dev/ttyACM0
if [[ "$#" -gt 0 ]]; then
    PORT="$1"
fi
set -eou pipefail
stty -F "$PORT" 0:4:cbd:0:3:1c:7f:15:4:0:1:0:11:13:1a:0:12:f:17:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0
cat "$PORT"
